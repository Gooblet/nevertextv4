# Changelog

Version 4 of NeverText: This version has massive improvements in the code to make it easier to work with
version number understanding:
x.y.z
x = Major Version release
y = Major functionalty completed
z = Feature changes
z when version < 1 = base game development. will turn into 0.1.0 and z = feature changes when core gameplay built

## version 0.0.1a: 7/11/2020

    - able to make a player character
    - setup initial top menu
    - staged some of the core gameplay
        - hardcoded a lot of the game for testing
    - player class and enemy class are setup

## version 0.0.1b: 7/12/2020

    - Setup top menu function
        - This may expand later, but is functional now.
    - Lore menu created.
        - Basic overview to prove functionality and help the other important areas be developed.
    - Inventory Window created
        - Shows bonus stats for the worn items currently. addtl features will come later

## version 0.0.1c: 7/16/2020

    - Basic combat setup
    - Test Enemy setup
    - Version 1 levelup functions working
    - Post combat setup for win and lose
        - bug where the calculations add decimal places... maybe increase the health by current Sta points?

## version 0.0.2: 7/17/2020

    - Fixed levelup function more basic but will display nicer
    - Help menu setup.
        - Game details to actually help player will come when i have the core functionality ironed out
    - Updated Inventory functionality
        - Added character sheet to functions
        - players can check their stats, items, weapons and armor in this menu
        - no additional actions at this time in this menu
    - Combat damage calculations now includes Atk bonus from weapon used
        - Balance issues with including AC. Will rework and add back in
    - Bug fixes regarding text and functions not displaying properly
        - Attack menu Fixed
        - Enemy attack message fixed
        - Home Menu fixed
    - finished Mage demo skills usage screen
        - this will be updated in the next version to include the logic for all classes
    - All combat menu items working
        - Early versions but they are functional
    - Mage class is currently in Alpha testing phase.
        - working on improving balance with this class as others are built in
    - Version 2 of combat has been staged
        - the same functionality as version 1, but the code is easier to maintain
        - as of 2.0, the only working class fully is the mage.
        - all other classes can only use melee combat.
    - Potion use now consumes a potion.
        - levelup system does NOT refresh potions.
        - buy potions from store.
    - Potion use still triggers enemy combat round if player successfully consumes potion
    - Added in Boomba's Store -- he likes pickles!
        - Boomba's store sells potions currently

## version 0.0.2b: 7/18/2020

    - Added in a rest function to restore player's hp and skill points.
        - this will eventually be modified to not make everything max health
        - random encounters can interrupt the resting
    - set enemy xp values closer to what live may be
        - testing the efficacy of the skills in combat
    - fixed levelup menu bug regarding enemy gold drop
        - if the enemy had no gold value assigned, the game would crash
        - this has been fixed
    - added admin commands for quick function testing

## version 0.1.0: 7/18/2020

    - Save and load functions are working well enough for testing purposes
        - all players will save their character sheet to a csv file.
        - each character will have its own csv file
    - cleaned up some of the code in the game

## version 0.1.1: 7/19/2020

    - changed the rest function so it does not restore all skill points
        - restores 2 skill points currently.
        - Additional passive skills might increase this
    - Added Meditate feature.
        - Restore skill points
        - accessible at level 5
        - does not restore health
    - Fixed menu bugs
        - some menus would crash the game if player entered a certain input
    - Added some basic info to the help menu
    - Added a number of enemies to the game
        - enemies for the 'Fight random enemy' function will select a random enemy
        - this will eventually be based on character level so you won't be overwhelmed
            - currently only low level enemies are in game to help with balancing
    - Added in some lore to the lore menu
        - I recommend players check this out if you have an interest in lore
        - note: the text is massive, if it doesn't fit your screen, scroll up a bit
        - this will eventually be fixed

## version 0.1.2: 7/20/2020

    - Balancing work
        - Increased skill damage for all classes
            - Mage got the largest boost to make up for the last of HP
        - decreased melee damage for the mage class
        - increased rogue melee damage
        - decreased rogue Stamina
    - You now lose EXP on death - Be careful out there!
        - This currently will be more impactful at lower levels
    - Minor text changes during combat to add a layer of immersion
    - Added Mage Shield skill to increase mage armor for a few turns
    - fixed the death function so players no longer have negative health after they die

## version 0.1.3: 7/21/2020

    - Added Tier 1 bosses
        - this will be shown in a separate menu for engaging enemies
        - Do NOT engage this boss until you are strong enough!
    - Added level tier info to help menu
    - Fixed some more text errors
    - reworked the combat to incorporate stat points further
        - Damage will increase for skills as player levels up
        - Melee combat has not changed, it used the same format as the skills do now
    - Nerfed player starting stats
        - low level players were doing too much damage on skills
    - Added Tier 2 bosses
        - Warning: They are not balanced yet, may be harder to kill for some classes
    - Fixed the run from combat function
        - players could fail to run away and it would not trigger the enemy to attack
        - players also never saw the failed to run text
    - Increased stats of some enemies
        - Low level enemies i used for testing were too weak to test the more complex skills
        - boosted the hp mainly, adjusted damage for some additional randomness to combat
    - Fixed bug with loading the enemies stats in the proper format

## version 0.1.4: 07/22/2020

    - Fixed bug with enemies stun duration not being called properly causing game to crash
    - Store setup with armor and weapons and potions
        - inventory system is not in place so players will lose current gear when buying a new piece or armor
            - tried to set it up to inform the player of their purchase so they don't make the wrong purchase
        - Currently the AC and dmg bonus do nothing. Still working on figuring out how to add weapon damage without making everything too easy...
            - combat formula is too simple at the moment and adding any math that might return a decimal breaks things and round isn't working
    - Formatted some text in different menues
    - revamped levelup function
        - fixed new level max health calculation (was not applying the new stamina to the max total health)
        - there is a different leveling line for each class now
            - all stats will not increase with each level, it will vary by class AND new level

## version 0.1.5: 7/24/2020

    - Added a delete savegame function
    - Reorganized some of the code
        - still testing the new org, if you find a bug let me know
        - better code practices done with this version, not yet complete
    - Fixed: Known bug: when reading enemy details from csv, sometimes ValueError returns... intermittent issue
        - changed the save function to pandas and openpyxl and numpy so i can keep the value type when saving/loading
    - Changed intro screen text to be nicer looking

## version 0.1.6: 6/19/2022

    - Added some more helper text during fights and other functions so it's more clear for players what to do
    - Fixed a few bugs in help menu to prevent game crash
    - Updated some outdated code with things i've learned
        - more details to follow in next revision
    - Updated combat menu
        - enemy stats should not be hidden during certain phases of combat
        - more clarity on what player can/needs to do
    - Made some more progress on the first official quest
        - nothing amazing yet but getting ideas together on what other functions I need to build out
        - this probably won't be done until many other game functions are in place
        - but it will be used for testing

## version 0.1.7: 12/19/2023 - IN PROGRESS
    - Finally added a requirements.txt
        - Slightly bloated but it works.
    - Fixed some text to make it more clear for players when input is needed on following functions:
        - Rest function
        - combat screens
    - Beginning rework on combat functions
        - Working on adding skills as player focuses on different things
            IE: using your main attack ability will increase combat prowess, while using skills will only increase the skills. 
            - Validating skills use a skill slot and is represented
            - IN PROGRESS: color warning systems (low on skills should show 'RED' when below 10% of remaining skill/spell points)
        - WARRIOR:
            - Bash skill rework
                - There is a chance for the skill to fail, all skills will have this and will fail less at higher levels
                - still working on balancing and damage overall. 
                - Lowered damage from bash -- this will reduce spam of the skill so it's more of a utility than a finisher.
                - This will be applied to all skills with some variation depending on class.
        - GENERAL COMBAT:
            - Working on a 'chance to miss' function that's more than just a random.randint() function so it happens less often as the player levels but also can happen more often as the enemy gets stronger. aka -- more dynamic