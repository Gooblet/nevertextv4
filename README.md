# NEVERTEXT

Text based RPG written in Python - Inspired by EverQuest and DnD
    this is still in a super early state.
    Some of the core functionality works.
    a lot of the stuff is hard coded and placeholder text

This is a simple text RPG for me to learn different stuff about Python.
This was originally a batch file i wrote back in 2014...
    you may see some similar format functionality to batch
    like, my main file is massive...still working on learning how
    to make python read other files properly while maintaining
    the variables from my player class and enemy class
Previous version of the 'game' used sqlite to save data but
i found issues with that since i wasn't hosting it,
it was all going to be local.
This version saves the characters to a .csv file.

## How to run

If you stumble across this game here is some info:

all text based, just run

```python
python nevertext.py
```

Or

```python
python3 nevertext.py
```

depending on your machine.
If you're using Windows, change the Clear Screen Function from:

```python
def clear():
        _ = os.system('clear')
```

to

```python
def clear():
        _ = os.system('cls')
```

- all characters are saved to a .csv in the same dir as the game
- load a character by typing the name of the character (case sensitive)
- combat works but I have only added in some of the Tier 1 enemies
- see the Changelog for everything i've done
- see the todo.txt for some of my thoughts and things i want to add in.
- feel free to fork this and have fun. If you change a ton to the code -- let me know!
- I would love to learn from people

## Upcomming updates

- more enemies (started)
- quest management (started)
- loot tables (started)
- travel (cities, towns, ruins, etc)
- more class skills
- more classes
- dungeons
- more organized code (started)
- i will be spliting things up into their own script files soon
- Class balancing (started)
- Bosses(started)
- Better code practices
    -- functions will return only relevant values and reduce number of changes in the
    variables, making it easier to diagnose bugs

## recent updates

- see Changelog
