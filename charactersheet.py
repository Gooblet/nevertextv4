import os
import sys


def playerSheet(playerig):
    #version 2 inventory system
    print("###################################### Stats ######################################")
    print(f"Str: {playerig.pstr:<13}\t|\tSta: {playerig.psta:<13}\t")
    print(f"Int: {playerig.pint:<13}\t|\tAgi: {playerig.pagi:<13}\t")
    print(f"Weapon: {playerig.weapon:<13}\t|\tAtk Bonus: {playerig.atkbonus:<13}\t")
    print(f"Armor: {playerig.armor:<10}\t|\tAC: {playerig.AC:<10}\t")
    print("###################################################################################")
    input("Press Enter to go back\n>> ")


if __name__ == '__main__':
    playerSheet()