'''
testing new assignment function
'''

import random

enemylist = ['a skeleton', 'an orc', 'an evil wizard']
enemydmg = [4, 6, 8]
enemyhp = [10, 20, 15]


def selectEnemy():
    playerhp = 50
    enemyname = random.choice(enemylist)
    if enemyname == 'a skeleton':
        enemydmg, enemyhp = 4, 10
    elif enemyname == 'an orc':
        enemydmg, enemyhp = 6, 20
    else:
        enemydmg, enemyhp = 8, 15
        
    print(f"{enemyname}, {enemydmg}, {enemyhp}")
    print(f"{enemyname} did {enemydmg} to you.")
    playerhp -= enemydmg
    print(f"{playerhp} / 50")

    
selectEnemy()