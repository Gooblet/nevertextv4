import pandas as pd
from openpyxl import load_workbook
import random
import numpy as np

xl = pd.ExcelFile(r'./enemylist.xlsx', index=False)
df = xl.parse("Sheet1")

random_subset = df.sample(n=1)

print(random_subset['name'].values[0])
print(random_subset['Level'].values[0])
print(random_subset['maxhealth'].values[0])
print(random_subset['Health'].values[0])
print(random_subset['maxskills'].values[0])
print(random_subset['currskills'].values[0])
print(random_subset['goldgain'].values[0])
print(random_subset['estr'].values[0])
print(random_subset['esta'].values[0])
print(random_subset['eint'].values[0])
print(random_subset['eagi'].values[0])
print(random_subset['itemdrop'].values[0])
print(random_subset['itemdesc'].values[0])
print(random_subset['itemworth'].values[0])
print(random_subset['expgain'].values[0])
print(random_subset['damage'].values[0])
print(random_subset['stun'].values[0])
print(random_subset['stunduration'].values[0])

