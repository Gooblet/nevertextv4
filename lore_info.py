'''
This script is going to contain all the in game lore
This is going to be massive walls of text from the website: elitegamerslounge.com/home/lore/
Ideally i could build a requests/bs4 script to pull the data down dynamically, but i have a wget copy of the site
and can just copy/paste the relevant stuff in here -- would be quicker
building bs4 stuff would give me more training on web scraping...
'''
import os
import sys


def clear(): 
        _ = os.system('clear')


def loreMain():
    clear()
    print("###################################################################################")
    print("#                                 Lore Menu                                       #")
    print("###################################################################################")
    print("1. Tome Of Ages\n2. Freeport(Your home city)\n3. The First Ogre-Dwarven War\n4. Founding of Crushbone")
    option = input(">> ")
    if option == '1':
        tomeOfAges()
    elif option == '2':
        freeport()
    elif option == '3':
        theFirstOgreDwarvenWar()
    elif option == '4':
        foundingCrushbone()
    else:
        input("Lore is being added all the time, check back later. Press ENTER")
        loreMain()




def tomeOfAges():
    with open('lore/tomeofages.txt', 'r') as f:
        print(f.read())

def freeport():
    with open('lore/Freeport.txt', 'r') as f:
        print(f.read())

def theFirstOgreDwarvenWar():
    with open('lore/ogredwarfwar.txt', 'r') as f:
        print(f.read())

def foundingCrushbone():
    with open('lore/foundingcrushbone') as f:
        print(f.read())


if __name__ == '__main__':
    loreMain()