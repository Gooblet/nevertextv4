"""
This is one of the first Quest modules that I am working on. The idea here is to send the player on a journey without holding their hand.
Some quests may be straightforward: "Go find X and kill them or talk to them
Others may require a little more work to figure out.
The whole point of these is to tell a story.
"""


import os
import sys
import time
import colorama
from colorama import Fore, Back, Style
colorama.init()

def clear():
    _ = os.system('clear')

def lucan():
    clear()
    #interaction function with lucan
    print("You enter the city of Freeport, the hustle and bustle of the largest port city in Norrath.")
    time.sleep(1)
    print("Walking through the front gate you see guards posted at multiple locations, ")
    print("and merchants with their carts peddling all kinds of goods.\n")
    time.sleep(1)
    print(Fore.CYAN)
    print("'You look like a fightin' sort.' an older man states, talking to you.")
    print("'If'n you're looking for some adventure, and coin, go to the Freeport Militia house'")
    print(Style.RESET_ALL)
    time.sleep(2)
    input("Press ENTER to continue")
    clear()
    print("You approach the militia house and go through the main door and are greeted by a terrifying looking man")
    print("(If he can be called a man, there is an odd feeling about his presence)")
    time.sleep(1)
    print("**Sir Lucan D'lere looks down upon you.**")
    print("**You feel you are not welcome here.**")
    print(Fore.RED)
    playerName = input("you there, what is your name? I've not seen you around these parts?\n>>> ")
    print(f"What do you want {playerName}?")
    print(Style.RESET_ALL)
    time.sleep(2)
    print("Looking for work?")
    print(f"I have nothing for you at this time.")
    print("Maybe seek out something of note and return to\nme, proving your skill. Then we may speak.")
    time.sleep(3)
    print(Fore.YELLOW)
    print("You suspect that there may be some information you could gather from the guards in the area.")
    print("Do you want to talk to some of the [guards], or would you like to go about your [journey?]")
    nextStep = input(">> ")
    print(Style.RESET_ALL)
    if nextStep == 'guards':
        print("Need to add dialogue here")
    else:
        print("You leave the guardhouse and head back out through The Commonlands")
        time.sleep(1)
        print("Probably for the best anway, Freeport is no place for your kind.")
    


if __name__ == '__main__':
    lucan()