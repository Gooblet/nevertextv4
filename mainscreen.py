from clearscreen import clear
from pyfiglet import Figlet as fg



def main():
    clear()
    # Main menu (start of the program)
    f = fg(font='standard')
    print("###################################################################################")
    print(f.renderText('               NeverText'))
    print("                     An EverQuest Inspired Game")
    print("                            Version 0.1.4")
    print("###################################################################################")
    print("Please make a selection below\n ")
    print("1. Start a new game")
    print("2. Load a saved game")
    print("3. Delete a saved game")
    print("4. Leave the game")
    option = input(">> ")
    if option == "1":
        newGame()
    elif option == "2":
        loadGame()
    elif option == "3":
        deleteGame()
    elif option == "4":
        exitGame()
    else:
        input("Select one of the options above. Press Enter to continue")
        main()