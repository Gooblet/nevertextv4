#!/usr/bin/env python
# -*- coding: utf-8 -*-
# title           :nevertext.py
# description     :This is the main file for NeverText
# author          : Nic Weilbacher
# date            : 7/22/2020
# version         : 0.1.6
# usage           : python(3) nevertext.py
# notes           : See Changelog.txt
# python_version  :3.7.0
# =======================================================================

import os
import time
import sys
import random
import lucandlere
import storemenu
import topmenu
import charactersheet
import enemies
import lore_info
import math
import csv  # current saving method... still a work in progress.
import pandas as pd # new save method -- work in progress
from pyfiglet import Figlet as fg
from openpyxl import load_workbook
import numpy as np
#from clearscreen import clear


#########################################################################
#                           Global Variables
#########################################################################

playerClasses = ['Warrior', 'Rogue', 'Mage']
classStr = [8, 6, 5]
classSta = [10, 8, 6]
classInt = [5, 5, 14]
classAgi = [7, 10, 5]
classWeapon = ['Rusty Sword', 'Rusty Dagger', 'Cracked Staff']
cwDamage = [3, 5, 2]
playerig = ''
clothArmor = ['Old Robes', 'Dark robes', 'Shining Robes']
clothAC = [2, 4, 8]
chainArmor = ['Old Chainmail', 'Dwarven Ringmail', 'Shadow Ringmail']
chainAC = [4, 9, 14]
plateArmor = ['Bronze Plate', 'Fine Plate', 'Cobalt Plate']
plateAC = [9, 15, 20]

activeQuests = ['Find the missing Locket', 'Stop the Gnolls', 'Explore the Commonlands']


# append new quests to this list?

#########################################################################
#                           Clear Screen Function
#########################################################################

def osdetect():
    """
    Operating system detection function

    This function detects the operating system running on the computer to execute the correct terminal commands

    """
    
    #global tclear
    platform = sys.platform
    tclear = ''
    platform
    if platform == 'darwin':
        tclear = 'clear'
        _ = os.system(tclear)
    elif platform == 'linux' or platform == 'linux32':
        tclear = 'clear'
        _ = os.system(tclear)
    elif platform == 'win32':
        tclear = 'cls'
        _ = os.system(tclear)
    else:
        print("Cannot determine what operating system you're running on. \nThis Application can run on linux, windows, and mac only")
        print("If you're running on a toaster,  please update to toaster2.0 running debian to play")
        input("press enter to exit the program and fix the error")
        sys.exit()

def clear():
    _ = os.system('clear')


#########################################################################
#                           Player Class
#########################################################################

class player:
    # this is the main player class
    def __init__(self):
        self.name = ''
        self.level = int()
        self.pclass = ''
        self.maxhealth = int()
        self.health = self.maxhealth
        self.maxskills = int()
        self.currentskills = self.maxskills
        self.potions = int()
        self.exp = int()
        self.levelupxp = int()
        self.pstr = int()
        self.pint = int()
        self.psta = int()
        self.pagi = int()
        self.gold = int()
        self.weapon = ''
        self.atkbonus = int()
        self.armor = ''
        self.AC = int()
        self.benefitBuff = int()
        self.buffDuration = int()


#########################################################################
#                           Enemy Class
#########################################################################

class enemy:
    # def __init__(self, name):
    name = ''
    maxhealth = int()
    health = maxhealth
    maxskills = int()
    skills = maxskills
    goldgain = int()
    estr = int()
    esta = int()
    eint = int()
    eagi = int()
    itemdrop = int()
    expgain = int()
    damage = int()
    level = int()
    stun = int()
    stunduration = int()


###################################################################################
#                   Main Menu
###################################################################################

def main():
    osdetect()
    # Main menu (start of the program)
    f = fg(font='standard')
    print("###################################################################################")
    print(f.renderText('               NeverText'))
    print("                     An EverQuest Inspired Game")
    print("                            Version 0.1.6")
    print("###################################################################################")
    print("Please make a selection below\n ")
    print("1. Start a new game")
    print("2. Load a saved game")
    print("3. Delete a saved game")
    print("4. Leave the game")
    option = input(">> ")
    if option == "1":
        newGame()
    elif option == "2":
        loadGame()
    elif option == "3":
        deleteGame()
    elif option == "4":
        exitGame()
    else:
        input("Select one of the options above. Press Enter to continue")
        main()


###################################################################################
#                   New Game Menu
###################################################################################

def newGame():
    # Check to see if the player wants a lore overview (Good for new players - not implimented fully yet)
    osdetect()
    print("###################################################################################")
    print("Welcome Adventurer, to Norrath.")
    print("If you would like an overview of this world, type: 'lore'")
    print("Otherwise, type: 'new'")
    print("###################################################################################")
    option = input(">> ")
    if option == 'new':
        newChar()
    elif option == 'lore':
        loreMenu()
    else:
        print("select the correct option")
        input("Press enter to try again")
        newGame()


def newChar():
    global playerig
    playerig = player()
    osdetect()
    global playerClasses, classStr, classSta, classInt, classAgi  # use this to call the game variables
    # Create the new character
    print("###################################################################################")
    print("Let's pick a class:")
    for a, b, c, d, e in zip(playerClasses, classStr, classSta, classInt, classAgi):
        print(f"'{a}': Str: {b} | Sta: {c} | Int: {d} | Dex: {e}")
    print("Select a class from the list above. Hint: Type the name in quotes. Example: 'Warrior', type Warrior(Case sensitive)")
    choice = input(">> ")
    if choice in playerClasses:
        osdetect()
        print(f"You have selected: {choice}")
        selectedClass = choice
        if selectedClass == 'Warrior' or selectedClass == 'warrior':
            playerig.pclass = 'Warrior'
            playerig.level += 1
            name = input("What Name should we call you? : >> ")
            playerig.name = name
            playerig.potions += 2
            playerig.exp += 0
            playerig.pstr += 6
            playerig.pint += 2
            playerig.psta += 10
            playerig.pagi += 4
            playerig.gold += 0
            playerig.levelupxp += 100
            healthcalc = playerig.pstr + random.randint(1, 15)
            playerig.maxhealth = int(healthcalc)
            playerig.health = playerig.maxhealth
            playerig.maxskills = 3
            playerig.currentskills = playerig.maxskills
            playerig.weapon = 'Rusty Sword'
            playerig.atkbonus = 3
            playerig.armor = 'Bronze Plate'
            playerig.AC = 9
            benefitBuff = 0
            buffDuration = 0

        elif selectedClass == 'Rogue' or selectedClass == 'rogue':
            playerig.pclass = 'Rogue'
            playerig.level += 1
            name = input("What Name should we call you? : >> ")
            playerig.name = name
            playerig.potions += 2
            playerig.exp += 0
            playerig.pstr += 4
            playerig.pint += 3
            playerig.psta += 6
            playerig.pagi += 9
            playerig.gold += 0
            playerig.levelupxp += 100
            healthcalc = playerig.pstr + random.randint(1, 15)
            playerig.maxhealth = int(healthcalc)
            playerig.health = playerig.maxhealth
            playerig.maxskills = 4
            playerig.currentskills = playerig.maxskills
            playerig.weapon = 'Rusty Dagger'
            playerig.atkbonus = 5
            playerig.armor = 'Old Chainmail'
            playerig.AC = 4
            benefitBuff = 0
            buffDuration = 0

        elif selectedClass == 'Mage' or selectedClass == 'mage':
            playerig.pclass = 'Mage'
            playerig.level += 1
            name = input("What Name should we call you? : >> ")
            playerig.name = name
            playerig.potions += 2
            playerig.exp += 0
            playerig.pstr += 2
            playerig.pint += 10
            playerig.psta += 5
            playerig.pagi += 5
            playerig.gold += 0
            playerig.levelupxp += 100
            healthcalc = playerig.pstr + random.randint(1, 15)
            playerig.maxhealth = int(healthcalc)
            playerig.health = playerig.maxhealth
            playerig.maxskills = 6
            playerig.currentskills = playerig.maxskills
            playerig.weapon = 'Cracked Staff'
            playerig.atkbonus = 2
            playerig.armor = 'Old Robes'
            playerig.AC = 2
            benefitBuff = 0
            buffDuration = 0

        osdetect()
        print("###################################################################################")
        print(f"{playerig.name}. You have chosen to be a {playerig.pclass}. Here are your stats:")
        print(f"Strength: {playerig.pstr}")
        print(f"Stamina: {playerig.psta}")
        print(f"Intelligence: {playerig.pint}")
        print(f"Agility: {playerig.pagi}")
        print("###################################################################################")
        print("You have the following items:")
        print(f"Gold: {playerig.gold}")
        print(f"Potions: {playerig.potions}")
        print(f"Weapon: {playerig.weapon}")
        print(f"Armor: {playerig.armor}")
        input("When you are satisfied with what you see, press ENTER to continue")
        homeMenu()
    else:
        print("You need to select one of the classes in the list.")
        print("More may be added for later versions of the game")
        input("Press ENTER to try again")
        newChar()


###################################################################################
#                   Lore Menu
###################################################################################

def loreMenu():
    osdetect()
    # alpha version of lore just to get the game working
    lore_info.loreMain()
    input("Press ENTER to return to the character creation screen")
    newGame()


###################################################################################
#                   Top Menu
###################################################################################


def topMenu(playerig):
    osdetect()
    topmenu.statMenu(playerig)


###################################################################################
#                   Home Menu
###################################################################################


def homeMenu():  # I should reorganize this menu

    osdetect()
    topmenu.statMenu(playerig)
    print("\n")
    print("What would you like to do?")
    print("1. Fight random enemy")
    print("B. Boss Fight")
    print("2. Character Sheet")
    print("3. Check Quests")
    print("4. Talk to a NPC")
    print("S. Store")
    print("R. Rest")
    print("M. Meditate")
    print("5. Help")
    print("6. Save Game")
    print("7. Exit")
    print("A. Admin console(testing purposes only, authentication required.)")
    print("T. Test things function call")
    option = input(">> ")
    if option == '1':
        selectEnemy()
    elif option == '2':
        charSheet(playerig)
    elif option == '3':
        questList()
    elif option == '4':
        npcChat(playerig)
    elif option == '5':
        helpMenu()
    elif option == '6':
        saveGame()
    elif option == '7':
        exitGame()
    elif option == 'A' or option == 'a':
        adminCommands()
    elif option == 's' or option == 'S':
        storeMain(playerig)
    elif option == 'r' or option == 'R':
        restMain()
    elif option == 'm' or option == 'M':
        meditate()
    elif option == 'b' or option == 'B':
        bossSelect()
    elif option == 't' or option == 'T':
        testfunctioncall(playerig)
    else:
        input("Press ENTER to select one of the options on the screen")
        homeMenu()


###################################################################################
#                   Fight Menu
###################################################################################

def selectEnemy():  # need to change this to enemies.py
    # setup enemy
    global enemyig
    enemyig = enemy()
    xl = pd.ExcelFile(r'./enemylist.xlsx')#, sheetname='Sheet1')
    xl1 = pd.read_excel(xl, 'Sheet1')
    df = xl.parse("Sheet1")
    random_subset = df.sample(n=1)
    
    enemyig.name = random_subset['name'].values[0]
    enemyig.level = random_subset['Level'].values[0]
    enemyig.maxhealth = random_subset['maxhealth'].values[0]
    enemyig.health = random_subset['Health'].values[0]
    enemyig.maxskills = random_subset['maxskills'].values[0]
    enemyig.skills = random_subset['currskills'].values[0]
    enemyig.goldgain = random_subset['goldgain'].values[0]
    enemyig.estr = random_subset['estr'].values[0]
    enemyig.esta = random_subset['esta'].values[0]
    enemyig.eint = random_subset['eint'].values[0]
    enemyig.eagi = random_subset['eagi'].values[0]
    enemyig.itemdrop = random_subset['itemdrop'].values[0]
    enemyig.itemdesc = random_subset['itemdesc'].values[0]
    enemyig.expgain = random_subset['expgain'].values[0]
    enemyig.dmg = random_subset['damage'].values[0]
    enemyig.stun = random_subset['stun'].values[0]
    enemyig.stunduration = random_subset['stunduration'].values[0]
    print(f"You were attacked by {enemyig.name}!")
    if enemyig.level > playerig.level:
        print("Looks like a difficult fight")
        print("Press ENTER to begin")
    elif enemyig.level == playerig.level:
        print("Looks like an even fight")
        print("Press ENTER to begin")
    else:
        print("looks like you could win this fight")
        print("Press ENTER to begin")
    input(">> ")
    mainFightScreen()



def bossSelect():
    global enemyig
    enemyig = enemy()
    osdetect()
    print("Which Tier of Boss would you like to fight?")
    print("1. Tier 1\n2. Tier 2\n3. Tier 3(Only tier 1 and 2 are in game)\n4. Tier 4\n5. Tier 5\n6. Tier 7\n8. Tier 8")
    option = input(">> ")
    if option == '1':
        xl = pd.ExcelFile(r'./bosslist1.xlsx')#), sheetname='Sheet1')
        xl1 = pd.read_excel(xl, 'Sheet1')
        df = xl.parse("Sheet1")
        random_subset = df.sample(n=1)
        
        enemyig.name = random_subset['name'].values[0]
        enemyig.level = random_subset['Level'].values[0]
        enemyig.maxhealth = random_subset['maxhealth'].values[0]
        enemyig.health = random_subset['Health'].values[0]
        enemyig.maxskills = random_subset['maxskills'].values[0]
        enemyig.skills = random_subset['currskills'].values[0]
        enemyig.goldgain = random_subset['goldgain'].values[0]
        enemyig.estr = random_subset['estr'].values[0]
        enemyig.esta = random_subset['esta'].values[0]
        enemyig.eint = random_subset['eint'].values[0]
        enemyig.eagi = random_subset['eagi'].values[0]
        enemyig.itemdrop = random_subset['itemdrop'].values[0]
        enemyig.itemdesc = random_subset['itemdesc'].values[0]
        enemyig.expgain = random_subset['expgain'].values[0]
        enemyig.dmg = random_subset['damage'].values[0]
        enemyig.stun = random_subset['stun'].values[0]
        enemyig.stunduration = random_subset['stunduration'].values[0]
        print(f"You have been challenged by {enemyig.name}, who is level {enemyig.level}. Good luck!")
        print("Press ENTER to begin")
        input(">> ")
        mainFightScreen()
    elif option == '2':
        xl = pd.ExcelFile(r'./bosslist2.xlsx')#, sheetname='Sheet1')
        xl1 = pd.read_excel(xl, 'Sheet1')
        df = xl.parse("Sheet1")
        random_subset = df.sample(n=1)
        
        enemyig.name = random_subset['name'].values[0]
        enemyig.level = random_subset['Level'].values[0]
        enemyig.maxhealth = random_subset['maxhealth'].values[0]
        enemyig.health = random_subset['Health'].values[0]
        enemyig.maxskills = random_subset['maxskills'].values[0]
        enemyig.skills = random_subset['currskills'].values[0]
        enemyig.goldgain = random_subset['goldgain'].values[0]
        enemyig.estr = random_subset['estr'].values[0]
        enemyig.esta = random_subset['esta'].values[0]
        enemyig.eint = random_subset['eint'].values[0]
        enemyig.eagi = random_subset['eagi'].values[0]
        enemyig.itemdrop = random_subset['itemdrop'].values[0]
        enemyig.itemdesc = random_subset['itemdesc'].values[0]
        enemyig.expgain = random_subset['expgain'].values[0]
        enemyig.dmg = random_subset['damage'].values[0]
        enemyig.stun = random_subset['stun'].values[0]
        enemyig.stunduration = random_subset['stunduration'].values[0]
        print(f"You have been challenged by {enemyig.name}, who is level {enemyig.level}. Good luck!")
        print("Press ENTER to begin")
        input(">> ")
        mainFightScreen()
    elif option == '3':
        pass
    elif option == '4':
        pass
    elif option == '5':
        pass
    elif option == '6':
        pass
    elif option == '7':
        pass
    elif option == '8':
        pass
    else:
        input("Please select one of the tiers in the list. ENTER to go back")
        bossSelect()
    # should be able to print the bosses out, load them from the csv
    # then go right into the mainFightScreen()?


def mainFightScreen():
    # this is where the fighting is going to happen.
    '''
    this section should have all the status changes to the variales from what the other functions are returnging
    goal is to minimize the object changes to as few functions as posssible
    '''
    osdetect()
    topmenu.statMenu(playerig)
    print("\n\n")
    print("###################################################################################")
    print(f"Enemy Name: {enemyig.name} | Enemy Level: {enemyig.level}")
    print(f"Enemy HP: {enemyig.health} / {enemyig.maxhealth}")
    print("###################################################################################")
    print("\nWhat would you like to do?\n1. Attack Melee\n2. Attack Skill\n3. Use Potion\n4. Run.")
    option = input(">> ")
    if option == '1':  # MELEE
        playerCombat()
    elif option == '2':  # SKILL USE
        playerSkillUse()
    elif option == '3':  # POTION
        itemUse()
    elif option == '4':  # RUN
        print("You attempt to run away")
        runNum = random.randint(0, playerig.pagi)
        failNum = random.randint(1, 3)
        if runNum <= failNum:
            print("You were unable to run away, your enemy is too fast!")
            input(">> ")
            enemyCombat()
        else:
            input("You have successfully run away!\n >>")
            homeMenu()
    else:
        input("Select one of the options above. Press Enter:")
        mainFightScreen()


def playerCombat():  # MELEE Combat only here
    osdetect()
    topmenu.statMenu(playerig)
    print("###################################################################################")
    print(f"Enemy Name: {enemyig.name} | Enemy Level: {enemyig.level}")
    print(f"Enemy HP: {enemyig.health} / {enemyig.maxhealth}")
    print("###################################################################################")
    print(f"You swing your {playerig.weapon} at {enemyig.name}")
    playerDamage = playerig.pstr + playerig.atkbonus
    enemyig.health -= playerDamage
    print(f"You have done {playerDamage} to {enemyig.name}")
    print("Press ENTER to continue")
    input(">> ")
    osdetect()
    if enemyig.health <= 0:
        battleWin()
    else:
        enemyCombat()


def playerSkillUse():
    '''
    fix this to not make me repeat the same lines each time
    playerig.skilluse:

    '''
    '''
    #skills = ['firebolt', 'lightning', 'mage shield']
    skillsd = {'Mage': ['firebolt', 'lightning', 'mage shield']}
    avail_skills = skillsd[playerig.pclass]
    print('which skills:\n' + ','.join(map(lambda skill,index:(index+1) + '. ' + skill, avail_skills ,[i for i in range(len(avail_skills))])))
    outputtext = ''
    for x in range(len(avail_skills)):
        outputtext += str(x+1) + '. ' + 
    '''
    osdetect()
    topmenu.statMenu(playerig)
    print("###################################################################################")
    print(f"Enemy Name: {enemyig.name} | Enemy Level: {enemyig.level}")
    print(f"Enemy HP: {enemyig.health} / {enemyig.maxhealth}")
    print("###################################################################################")

    if playerig.pclass == 'Mage':
        print("Which skill:\n1. Firebolt\n2. Lightning\n3. Mage Shield")
        option = input(">> ")
        if option == '1':
            if playerig.currentskills > 0:
                fireBallDmg = random.randint(2, 4) + playerig.pint
                enemyig.health = enemyig.health - fireBallDmg
                print(f"you have done {fireBallDmg} Fire damage to {enemyig.name}!\n")
                playerig.currentskills -= 1
                input(">>")
                if enemyig.health <= 0:
                    battleWin()  # def check enemy and player health
                else:
                    enemyCombat()
            else:
                print("You don't have the skill points needed to use a skill")
                input(">> ")
                mainFightScreen()
        elif option == '2':
            if playerig.currentskills > 0:
                lightningDmg = random.randint(1, 3) + playerig.pint
                enemyig.health = enemyig.health - lightningDmg
                print(f"you have done {lightningDmg} Lightning damage to {enemyig.name}!\n")
                playerig.currentskills -= 1
                input(">>")
                if enemyig.health <= 0:
                    battleWin()
                else:
                    enemyCombat()
            else:
                print("You don't have the skill points needed to use a skill")
                input(">> ")
                mainFightScreen()
        elif option == '3':
            if playerig.currentskills > 0:
                playerig.buffDuration += 3
                playerig.benefitBuff += 1 + playerig.pstr
                print(f"You cast Mage Shield on yourself. Increasing your spell Armor by {playerig.benefitBuff}\n")
                playerig.currentskills -= 1
                input(">>")
                if enemyig.health <= 0:
                    battleWin()
                else:
                    enemyCombat()
            else:
                print("You don't have the skill points needed to use a skill")
                input(">> ")
                mainFightScreen()
        else:
            input("select one of the options listed above for your class. ENTER to try again")
            # moving this to a function will allow for less backtracking and button clicking to use the atk again.
            mainFightScreen()
    elif playerig.pclass == 'Warrior':
        print("Which skill:\n1. Shoulder Bash\n2. Kick")
        option = input(">> ")
        if option == '1':
            if playerig.currentskills > 0:
                bashSuccess = random.randint(0, 10) + playerig.pstr
                '''
                Testing adding a function to nerf this skill. currently this skill always stuns... which we don't want. there needs to be some danger. 
                - UPDATE: needs minor tweaking but is getting closer
                '''
                if bashSuccess >= 11:
                    shoulderDmg = random.randint(0, 3)
                    enemyig.health = enemyig.health - shoulderDmg
                    print(f"you have done {shoulderDmg} Physical damage to {enemyig.name}!\n")
                    playerig.currentskills -= 1
                    enemyig.stun = 1
                    enemyig.stunduration = 2
                    print(f"{enemyig.name} is stunned!")
                    input(">>")
                    if enemyig.health <= 0:
                        battleWin()
                    else:
                        enemyCombat()
                else:
                    print("Your bash skill failed! You missed!")
                    enemyCombat()
            else:
                print("You don't have the skill points needed to use a skill")
                input(">> ")
                mainFightScreen()
        elif option == '2':
            if playerig.currentskills > 0:
                kickDmg = random.randint(3, 6) + playerig.pstr
                enemyig.health = enemyig.health - kickDmg
                print(f"you have done {kickDmg} Physical damage to {enemyig.name}!\n")
                playerig.currentskills -= 1
                input(">>")
                if enemyig.health <= 0:
                    battleWin()
                else:
                    enemyCombat()
            else:
                print("You don't have the skill points needed to use a skill")
                input(">> ")
                mainFightScreen()
        else:
            input("select one of the options listed above for your class. ENTER to try again")
            # moving this to a function will allow for less backtracking and button clicking to use the atk again.
            mainFightScreen()
    elif playerig.pclass == 'Rogue':
        print("Which skill:\n1. Backstab\n2. Double Attack")
        option = input(">> ")
        if option == '1':
            if playerig.currentskills > 0:
                backstabDmg = random.randint(2, 3) + playerig.pagi
                enemyig.health = enemyig.health - backstabDmg
                print(f"you have done {backstabDmg} Physical damage to {enemyig.name}!\n")
                playerig.currentskills -= 1
                input(">>")
                if enemyig.health <= 0:
                    battleWin()
                else:
                    enemyCombat()
            else:
                print("You don't have the skill points needed to use a skill")
                input(">> ")
                mainFightScreen()
        elif option == '2':
            if playerig.currentskills > 0:
                DoubleAtkDmg = random.randint(1, 3) + playerig.pagi * 2
                enemyig.health = enemyig.health - DoubleAtkDmg
                print(f"You pull our your daggers and stab {enemyig.name} twice!")
                print(f"you have done {DoubleAtkDmg} Physical damage to {enemyig.name}!\n")
                playerig.currentskills -= 1
                input(">>")
                if enemyig.health <= 0:
                    battleWin()
                else:
                    enemyCombat()
            else:
                print("You don't have the skill points needed to use a skill")
                input(">> ")
                mainFightScreen()
        else:
            input("select one of the options listed above for your class. ENTER to try again")
            # moving this to a function will allow for less backtracking and button clicking to use the atk again.
            mainFightScreen()
    elif playerig.pclass == 'Cleric':  # this class isn't in the game yet but i'm keeping this here
        print("Which skill:\n1. Heal\n2. Smite")
        option = input(">> ")
        if option == '1':
            if playerig.currentskills > 0:
                healAmt = random.randint(1, 5) + playerig.pint
                playerig.health += healAmt
                print(f"you have healed yourself for {healAmt} Hit Points!\n")
                if playerig.health >= playerig.maxhealth:
                    playerig.health = playerig.maxhealth
                playerig.currentskills -= 1
                input(">>")
                if enemyig.health <= 0:
                    battleWin()
                else:
                    enemyCombat()
            else:
                print("You don't have the skill points needed to use a skill")
                input(">> ")
                mainFightScreen()
        elif option == '2':
            if playerig.currentskills > 0:
                smiteDmg = random.randint(2, 5)
                enemyig.health = enemyig.health - DoubleAtkDmg
                print(f"you have done {smiteDmg} Holy damage to {enemyig.name}!\n")
                playerig.currentskills -= 1
                input(">>")
                if enemyig.health <= 0:
                    battleWin()
                else:
                    enemyCombat()
            else:
                print("You don't have the skill points needed to use a skill")
                input(">> ")
                mainFightScreen()
        else:
            input("select one of the options listed above for your class. ENTER to try again")
            # moving this to a function will allow for less backtracking and button clicking to use the atk again.
            mainFightScreen()
    else:
        pass


def enemyCombat():  # testing this
    '''
    return the damage number and go back to the main combat screen
    This will be improved upon in future revisions once I give enemies their own skills
    '''
    osdetect()
    topmenu.statMenu(playerig)
    print("###################################################################################")
    print(f"Enemy Name: {enemyig.name} | Enemy Level: {enemyig.level}")
    print(f"Enemy HP: {enemyig.health} / {enemyig.maxhealth}")
    print("###################################################################################")
    if enemyig.stunduration > 0:
        print(f"{enemyig.name} is stunned and cannot attack this turn")
        enemyig.stunduration -= 1
        print("Press ENTER to continue")
        input(">> ")
        mainFightScreen()
    else:
        enemyDmg = enemyig.dmg - playerig.benefitBuff
        if enemyDmg <= 0:
            enemyDmg = 0
        print(f"{enemyig.name} does {enemyDmg} damage to you!\n")
        playerig.health -= enemyDmg
        if playerig.benefitBuff > 0:
            playerig.buffDuration -= 1
            if playerig.buffDuration == 0:
                playerig.benefitBuff = 0
        print("Press ENTER to continue")
        input(">> ")
        if playerig.health <= 0:
            battleLose()
        else:
            mainFightScreen()


def itemUse():
    print("Do you want to use a Potion to heal yourself?\n Y/N")
    potionUse = input(">> ")
    if potionUse == 'y' or potionUse == 'Y':
        potionHealAmt = random.randint(20, 55)
        print(f"You have healed yourself for {potionHealAmt}!")
        playerig.health += potionHealAmt
        if playerig.health > playerig.maxhealth:
            playerig.health = playerig.maxhealth
            playerig.potions -= 1
            print("Press ENTER to continue")
            input(">> ")
            enemyCombat()
        else:
            print("Unable to apply the potion for some reason. DEV NOTE -- this will be fixed in future updates")
            print("Press ENTER to continue")
            input(">> ")
            mainFightScreen()


def battleWin():
    print(f"You have defeated {enemyig.name}!")
    print(f"You have gained {enemyig.expgain} experience points!")
    playerig.exp += enemyig.expgain
    playerig.gold += enemyig.goldgain
    input("Press ENTER to continue")
    if playerig.exp >= playerig.levelupxp:
        levelup()
    else:
        homeMenu()


def battleLose():
    print(f"you were defeated by {enemyig.name}")
    print(f"You lost some EXP!")
    playerig.health = playerig.maxhealth
    playerig.exp -= 25 # playerid.exp * 0.80 (would this take 20% off?)
    #TODO: Make this a % of current exp so players don't go into the negatives or lose a level
    if playerig.exp <= 0:
        playerig.exp = 0 # basic bitch solution
    input("Press ENTER if you dare try again")
    homeMenu()


###################################################################################
#                   Inventory Menu
###################################################################################

def charSheet(playerig):
    osdetect()
    topmenu.statMenu(playerig)
    charactersheet.playerSheet(playerig)
    homeMenu()


def inspectItem():
    pass


###################################################################################
#                   Store Menu
###################################################################################

def testfunctioncall(playerig):
    osdetect()
    storemenu.testthings(playerig)
    homeMenu()


def storeMain(playerig):  # storemenu.py
    osdetect()
    topmenu.statMenu(playerig)
    storemenu.storeMenu(playerig)
    print("\n")
    homeMenu()


def sellMenu():
    input("Not added yet>>")
    storeMain()


###################################################################################
#                   Quest Menu
###################################################################################

def questList():
    osdetect()
    topmenu.statMenu(playerig)
    global activeQuests
    print("Quest Screen")
    for q in activeQuests:
        print(q)
    input("Press ENTER to go back")
    homeMenu()


def quests():
    # this might be moved to a NPC sub function
    pass


###################################################################################
#                   NPC Menu
###################################################################################
# this is just a placeholder for more stuff later
def npcChat(playerig):
    osdetect()
    topmenu.statMenu(playerig)
    lucandlere.lucan(playerig)
    print("\n")
    input("Press Enter to return")
    homeMenu()


###################################################################################
#                   Loot Menu
###################################################################################

def loot():
    '''
    need to set list of loot that i can pull from easily. then assign the bonuses
    from each loot piece, and the sell value
    and setup the ability to sell the items
    and an inventory to manage the items...
    could add a list variable to the player class for inventory and append looted items
    to that and then make another variable for the item's value and anything else
    relevant to the items i'm dropping.
    '''
    pass


###################################################################################
#                   Rest Menu
###################################################################################
# going to encorporate a random encounter chance soon. need to work it out first

def restMain():
    osdetect()
    topmenu.statMenu(playerig)
    if playerig.health == playerig.maxhealth:
        print("You are fully rested, you would not benefit from an additional rest right now")
        input(">> ")
        homeMenu()
    else:
        print("You pull out your sleeping sack, prop yourself up on a rock\nand drift off to sleep.")
        playerig.currentskills += 2
        playerig.health = playerig.maxhealth
        time.sleep(4)
        if playerig.currentskills >= playerig.maxskills:
            playerig.currentskills = playerig.maxskills
            input("You wake up a few hours later to some footsteps nearby, time to packup and move. \nPress ENTER to continue\n>>")
        else:
            input("You wake up a few hours later to some footsteps nearby, time to packup and move. \nPress ENTER to continue\n>>")
        homeMenu()


###################################################################################
#                   Quest Menu
###################################################################################
# player must be level 5 before they can use this function

def meditate():
    # restore player's skill points
    osdetect()
    print("############################### Meditate ##########################################")
    if playerig.level < 5:
        print("You are not skilled enough to use this yet. You require more training")
        input("Press ENTER to return")
        homeMenu()
    else:
        playerig.currentskills = playerig.maxskills
        print("You find a quiet place to sit.\n")
        time.sleep(2)
        print("You fold your hands in your lab and close your eyes")
        time.sleep(2)
        print("You quiet your mind and over time you feel your energy restore itself")
        time.sleep(4)
        input("You have restored your skill points. Press ENTER to exit")
        homeMenu()


###################################################################################
#                   Levelup Menu
###################################################################################

def levelup():  # version 2.0
    """Levelup function within game
    current levelup system is hard coded and the same each time
    this makes leveling up less of a focus and armor more of a focus as 
    levels increase... am I okay with this?
    Need to find a way to increase things in a different way. Skills vs stats vs abilities. need more complex combat maths
    """
    
    print("DING! You have leveled up!")
    if playerig.pclass == 'Warrior':  # Warrior Level up Path
        print(
            f"Current Stats\nLevel: {playerig.level} | Max HP: {playerig.maxhealth} | Strength: {playerig.pstr} | Stamina: {playerig.psta} | Agility: {playerig.pagi} | Intelligence: {playerig.pint} | Max Skills: {playerig.maxskills}\n")
        print("###################################################################################")
        playerig.level += 1
        # Warrior will get a lot more HP than the other classes. every level = more stamina right now
        if playerig.level % 4 == 0:  # every 4 levels, increase non primary stats
            playerig.psta = playerig.psta + 1
            playerig.maxhealth += playerig.psta
            playerig.pagi = playerig.pagi + 1
            playerig.pint = playerig.pint + 1
            playerig.maxskills += 1
            playerig.currentskills = playerig.maxskills
            playerig.exp = 0
            playerig.health = playerig.maxhealth
            playerig.levelupxp = playerig.levelupxp * 2
            playerig.levelupxp = math.ceil(playerig.levelupxp)
            print(
                f"New Stats\nLevel: {playerig.level} | Max HP: {playerig.maxhealth} | Strength: {playerig.pstr} | Stamina: {playerig.psta} | Agility: {playerig.pagi} | Intelligence: {playerig.pint} | Max Skills: {playerig.maxskills}")
            input("levelup calculations complete - Press ENTER to see your new stats")

        if playerig.level % 2 == 0:  # every 2 levels, increase secondary stats
            playerig.pstr = playerig.pstr + 1
            playerig.psta = playerig.psta + 1
            playerig.maxhealth += playerig.psta
            playerig.maxskills += 1
            playerig.currentskills = playerig.maxskills
            playerig.exp = 0
            playerig.health = playerig.maxhealth
            playerig.levelupxp = playerig.levelupxp * 2
            playerig.levelupxp = math.ceil(playerig.levelupxp)  # why do i have this still? does it serve a purpose?
            print(
                f"New Stats\nLevel: {playerig.level} | Max HP: {playerig.maxhealth} | Strength: {playerig.pstr} | Stamina: {playerig.psta} | Agility: {playerig.pagi} | Intelligence: {playerig.pint} | Max Skills: {playerig.maxskills}")
            input("levelup calculations complete - Press ENTER to see your new stats")

        else:  # Every level, increase primary stats
            playerig.psta = playerig.psta + 1
            playerig.maxhealth += playerig.psta
            playerig.maxskills += 1
            playerig.currentskills = playerig.maxskills
            playerig.exp = 0
            playerig.health = playerig.maxhealth
            playerig.levelupxp = playerig.levelupxp * 2
            playerig.levelupxp = math.ceil(playerig.levelupxp)
            print(
                f"New Stats\nLevel: {playerig.level} | Max HP: {playerig.maxhealth} | Strength: {playerig.pstr} | Stamina: {playerig.psta} | Agility: {playerig.pagi} | Intelligence: {playerig.pint} | Max Skills: {playerig.maxskills}")
            input("levelup calculations complete - Press ENTER to see your new stats")

    elif playerig.pclass == 'Mage':  # Mage Level up Path

        print(
            f"Current Stats\nLevel: {playerig.level} | Max HP: {playerig.maxhealth} | Strength: {playerig.pstr} | Stamina: {playerig.psta} | Agility: {playerig.pagi} | Intelligence: {playerig.pint} | Max Skills: {playerig.maxskills}\n")
        print("###################################################################################")
        playerig.level += 1
        if playerig.level % 4 == 0:
            playerig.pstr = playerig.pstr + 1
            playerig.maxhealth += playerig.psta
            playerig.pagi = playerig.pagi + 1
            playerig.maxskills += 1
            playerig.currentskills = playerig.maxskills
            playerig.exp = 0
            playerig.health = playerig.maxhealth
            playerig.levelupxp = playerig.levelupxp * 2
            playerig.levelupxp = math.ceil(playerig.levelupxp)
            print(
                f"New Stats\nLevel: {playerig.level} | Max HP: {playerig.maxhealth} | Strength: {playerig.pstr} | Stamina: {playerig.psta} | Agility: {playerig.pagi} | Intelligence: {playerig.pint} | Max Skills: {playerig.maxskills}")
            input("levelup calculations complete - Press ENTER to see your new stats")
        elif playerig.level % 2 == 0:
            playerig.psta = playerig.psta + 1
            playerig.maxhealth += playerig.psta
            playerig.pint = playerig.pint + 1
            playerig.maxskills += 1
            playerig.currentskills = playerig.maxskills
            playerig.exp = 0
            playerig.health = playerig.maxhealth
            playerig.levelupxp = playerig.levelupxp * 2
            playerig.levelupxp = math.ceil(playerig.levelupxp)
            print(
                f"New Stats\nLevel: {playerig.level} | Max HP: {playerig.maxhealth} | Strength: {playerig.pstr} | Stamina: {playerig.psta} | Agility: {playerig.pagi} | Intelligence: {playerig.pint} | Max Skills: {playerig.maxskills}")
            input("levelup calculations complete - Press ENTER to see your new stats")
        else:
            playerig.maxhealth += playerig.psta
            playerig.pint = playerig.pint + 1
            playerig.maxskills += 1
            playerig.currentskills = playerig.maxskills
            playerig.exp = 0
            playerig.health = playerig.maxhealth
            playerig.levelupxp = playerig.levelupxp * 2
            playerig.levelupxp = math.ceil(playerig.levelupxp)
            print(
                f"New Stats\nLevel: {playerig.level} | Max HP: {playerig.maxhealth} | Strength: {playerig.pstr} | Stamina: {playerig.psta} | Agility: {playerig.pagi} | Intelligence: {playerig.pint} | Max Skills: {playerig.maxskills}")
            input("levelup calculations complete - Press ENTER to see your new stats")

    elif playerig.pclass == 'Rogue':  # Rogue Level up Path
        print(
            f"Current Stats\nLevel: {playerig.level} | Max HP: {playerig.maxhealth} | Strength: {playerig.pstr} | Stamina: {playerig.psta} | Agility: {playerig.pagi} | Intelligence: {playerig.pint} | Max Skills: {playerig.maxskills}\n")
        print("###################################################################################")
        playerig.level += 1
        if playerig.level % 4 == 0:
            playerig.psta = playerig.psta + 1
            playerig.maxhealth += playerig.psta
            playerig.pint = playerig.pint + 1
            playerig.maxskills += 1
            playerig.currentskills = playerig.maxskills
            playerig.exp = 0
            playerig.health = playerig.maxhealth
            playerig.levelupxp = playerig.levelupxp * 2
            playerig.levelupxp = math.ceil(playerig.levelupxp)
            print(
                f"New Stats\nLevel: {playerig.level} | Max HP: {playerig.maxhealth} | Strength: {playerig.pstr} | Stamina: {playerig.psta} | Agility: {playerig.pagi} | Intelligence: {playerig.pint} | Max Skills: {playerig.maxskills}")
            input("levelup calculations complete - Press ENTER to see your new stats")
        elif playerig.level % 2 == 0:
            playerig.pstr = playerig.pstr + 1
            playerig.maxhealth += playerig.psta
            playerig.pagi = playerig.pagi + 1
            playerig.maxskills += 1
            playerig.currentskills = playerig.maxskills
            playerig.exp = 0
            playerig.health = playerig.maxhealth
            playerig.levelupxp = playerig.levelupxp * 2
            playerig.levelupxp = math.ceil(playerig.levelupxp)
            print(
                f"New Stats\nLevel: {playerig.level} | Max HP: {playerig.maxhealth} | Strength: {playerig.pstr} | Stamina: {playerig.psta} | Agility: {playerig.pagi} | Intelligence: {playerig.pint} | Max Skills: {playerig.maxskills}")
            input("levelup calculations complete - Press ENTER to see your new stats")
        else:
            playerig.maxhealth += playerig.psta
            playerig.pagi = playerig.pagi + 1
            playerig.maxskills += 1
            playerig.currentskills = playerig.maxskills
            playerig.exp = 0
            playerig.health = playerig.maxhealth
            playerig.levelupxp = playerig.levelupxp * 2
            playerig.levelupxp = math.ceil(playerig.levelupxp)
            print(
                f"New Stats\nLevel: {playerig.level} | Max HP: {playerig.maxhealth} | Strength: {playerig.pstr} | Stamina: {playerig.psta} | Agility: {playerig.pagi} | Intelligence: {playerig.pint} | Max Skills: {playerig.maxskills}")
            input("levelup calculations complete - Press ENTER to see your new stats")
    homeMenu()


###################################################################################
#                   Save Game Menu
###################################################################################
'''
Make a csv file per player? 
this could make the directory unnecessarily large but it would be easy
just type the player name and find the playernamefile.csv to load
'''
def saveGame():
    osdetect()
    savedata = {'Name':[playerig.name], 'Class':[playerig.pclass], 'Level':[playerig.level], 'Potions':[playerig.potions], 'Experience':[playerig.exp], 'Strength':[playerig.pstr], 'Intelligence':[playerig.pint], 'Stamina':[playerig.psta], 'Agility':[playerig.pagi], 'Gold':[playerig.gold], 'LevelupXP':[playerig.levelupxp], 'MaxHealth':[playerig.maxhealth], 'health':[playerig.health], 'maxskill':[playerig.maxskills], 'currentskills':[playerig.currentskills], 'weapon':[playerig.weapon], 'armor':[playerig.armor], 'atkbonus':[playerig.atkbonus], 'AC':[playerig.AC], 'Buffs':[playerig.benefitBuff], 'Buff Duration':[playerig.buffDuration]}
    df = pd.DataFrame(savedata)
    filename = playerig.name + '_save.xlsx'
    df.to_excel('./' + filename, index=False)
    print(savedata)
    print("You have successfully saved the game")
    print("Press ENTER to go back to the main menu.")
    input(">> ")
    homeMenu()


###################################################################################
#                   Load Game Menu
###################################################################################

def loadGame():
    global playerig
    osdetect()
    
    loadplayername = input("Type the name of the character: ")
    loadfilename = str(loadplayername + "_save.xlsx")
    xl = pd.ExcelFile(r'./' + loadfilename)#, sheetname='Sheet1')
    xl1 = pd.read_excel(xl, 'Sheet1')
    df = xl.parse("Sheet1")
    playerig = player()

    playerig.name = df['Name'][0]
    playerig.pclass = df['Class'][0]
    playerig.level = df['Level'][0]
    playerig.potions = df['Potions'][0]
    playerig.exp = df['Experience'][0]
    playerig.pstr = df['Strength'][0]
    playerig.pint = df['Intelligence'][0]
    playerig.psta = df['Stamina'][0]
    playerig.pagi = df['Agility'][0]
    playerig.gold = df['Gold'][0]
    playerig.levelupxp = df['LevelupXP'][0]
    playerig.maxhealth = df['MaxHealth'][0]
    playerig.health = df['health'][0]
    playerig.maxskills = df['maxskill'][0]
    playerig.currentskills = df['currentskills'][0]
    playerig.weapon = df['weapon'][0]
    playerig.armor = df['armor'][0]
    playerig.atkbonus = df['atkbonus'][0]
    playerig.AC = df['AC'][0]
    playerig.benefitBuff = df['Buffs'][0]
    playerig.buffDuration = df['Buff Duration'][0]
    homeMenu()


###################################################################################
#                   Delete Game Menu
###################################################################################
def deleteGame():  # Needs fixed
    print("Do you want to delete a save file?\nY/N")
    deleteconfirm = input(">> ")
    if deleteconfirm == 'Y' or deleteconfirm == 'y':
        playername = input("Type the name of the character: ")
        filename = str(playername + ".csv")
        f = open(filename)
        csv_f = csv.reader(f)
        for row in csv_f:
            print(row[0:])
        print(f"Are you sure you want to delete {playername}?\nY/N")
        option = input(">> ")
        if option == 'y' or option == 'Y':
            if os.path.isfile(filename):
                os.remove(filename)
                main()
        else:
            print("Save file not found. Make sure you typed the character's name correctly")
            deleteGame()
    else:
        main()


###################################################################################
#                   Help Menu
###################################################################################

def helpMenu():
    # Basic information menu for the game
    osdetect()
    print("Help Menu")
    print("What would you like more information on?")
    print("1. Navigation help\n2. Combat\n3. Game Overview\n4. Level Tiers\n5. return to main screen")
    option = input(">> ")
    if option == '1':
        print("navigation help will go here")
        print("""\n
        Follow the on screen prompts and hit ENTER to navigate menus
        If you are not sure what to press, just try hitting ENTER
        If you see '>>' on the screen and there is no prompt above, just hit ENTER
        \n""")
        input("Press enter to return to help menu")
        helpMenu()
    elif option == '2':
        print("combat basics")
        print("""\n
        Combat is broken into a few parts. When you engage the enemy, you
        are given a few options: 1. Melee attack, 2. Skills combat, 3. use an item. 
        melee damage is based on the weapon you have equiped. The better the weapon, the 
        more damage you will do. Skills combat is based on the class you chose. 
        Some skills do more damage, some skills do status effects do you or the enemy. 
        The main thing to note is that you have a limited numeber of skills to use. 
        You can get your skill points by resting, meditating, or leveling up.
        Items are all different, read the description of the items in your inventory
        before going out to combat. Just note, that using an item still lets the 
        enemy take their turn.\n""")
        input("Press enter to return to help menu")
        helpMenu()
    elif option == '3':
        print("game overview")
        print("""\n
        Welcome to NeverText. This is an EverQuest and DnD inspired game. 
        Most of this game was developed so i could get better at Python.
        There is a lot of accurate lore from EverQuest in this game.
        Most of this game is navigated with numbers, letters, and ENTER.

        \n""")
        input("Press enter to return to help menu")
        helpMenu()
    elif option == '4':
        print("""
        The level tiers are designed to help players navigate the world
        and fight enemies around your level. The tiers are as follows:
        Tier 1: 1-5
        Tier 2: 6-10
        Tier 3: 11-15
        Tier 4: 16-20
        Tier 5: 21-30
        Tier 6: 31-40
        Tier 7: 41-49
        Tier 8: 50
        Each tier will bring harder enemies, harder bosses, and better loot!
        Don't just grind through levels, you will need to do quests to get
        better loot if you want to have a chance to win at the higher tiers.
        """)
        input("Press enter to return to help menu")
        helpMenu()
    elif option == '5':
        homeMenu()
    else:
        input("select option above. Enter to try again")
        helpMenu()


###################################################################################
#                   Exit Function
###################################################################################

def exitGame():
    osdetect()
    print("###################################################################################")
    print(" Are you sure you want to quit Norrath?\nY/N")
    print("###################################################################################")
    option = input(">> ")
    if option.lower() == 'y':
        print("Now leaving Norrath. Good luck adventurer.")
        time.sleep(1)
        osdetect()
        sys.exit()
    else:
        main()


###################################################################################
#                   Admin Console
###################################################################################

def adminAuth():
    pass


def adminCommands():
    # developer functions
    osdetect()
    topmenu.statMenu(playerig)
    print("Dev options -- if you are not a developer, turn back now!")
    print("1. heal\n2. addxp\n3. addgold\n4. back\n")
    devoption = input(">> ")
    if devoption == '1':
        playerig.health = playerig.maxhealth
        adminCommands()
    elif devoption == '2':
        playerig.exp += 100
        if playerig.exp >= playerig.levelupxp:
            levelup()
        else:
            adminCommands()
    elif devoption == '3':
        playerig.gold += 100000
        adminCommands()
    else:
        homeMenu()
    input("nothing yet. ENTER to go back")


###################################################################################
#                   Initialize Game
###################################################################################
# currently not used. might remove
def initfunction():
    # checks for a save file and loads it. otherwise, new game
    global game_state

    if not os.path.isfile(SAVEGAME_FILENAME):
        game_state = main()
    else:
        game_state = loadGame()
    homeMenu()


if __name__ == '__main__':
    main()
