import os
import sys
'''
how do i make this menu better?
instead of manually adding in each if statement for each new item, can i do a loop to print out and do the calculation

'''

def storeMenu(playerig):
    os.system('clear')
    armorList = ['Dusty Tunic', 'Dark robes', 'Shining Robes', 'Dwarven Ringmail', 'Shadow Ringmail', 'Fine Plate', 'Cobalt Plate']
    armorCost = [25, 500, 1000, 500, 1000, 750, 1250]
    acBonus = [1, 4, 8, 9, 14, 15, 20]
    aclassuse = ['Any', 'Mage', 'Mage', 'Rogue', 'Rogue', 'Warrior', 'Warrior']
    weaponList = ['Iron Sword', 'Steel Sword', 'Iron Daggers', 'Steel Daggers', 'Sacrificial Dagger']
    weaponCost = [350, 600, 350, 700, 500]
    wclassuse = ['Warrior', 'Warrior', 'Rogue', 'Rogue', 'Mage']
    dmgBonus = [5, 9, 9, 13, 6]
    itemList = ['Healing Potion', 'Greater Healing Potion']
    itemCost = [25, 125]
    itemCount = 1
    print("\n\n")
    print("Welcome to Boomba's store! Me sell pickles of all kinds!")
    print("Me have Dwarf pickles, Elf pickles, hooman pickles, gnome pickles... me have it all!\n")
    print("**Boomba the Big is a massive Ogre who has turned into a salesman... what would you like to buy**\n")
    print(f"\nYour Gold: {playerig.gold}")
    for a, b, c, d in zip(armorList, armorCost, aclassuse, acBonus):
        print(f"{itemCount}. {a}: {b} Gold - AC Bonus: {d} - Class: {c}") #TESTING
        itemCount += 1
    for d, e, f, g in zip(weaponList, weaponCost, wclassuse, dmgBonus):
        print(f"{itemCount}. {d}: {e} Gold - Damage Bonus: {g} - Class: {f}") #TESTING
        itemCount += 1
    for g, h in zip(itemList, itemCost):
        print(f"{itemCount}. {g}: {h} Gold") #TESTING
        itemCount += 1
        #finish the rest below and fix the above prints
    print("S. Sell")
    print("B. Back")
    '''
    choice = input(">> ")
    if choice in str(itemCount): #This does not work because itemcount = the last number which is 14.. list itemCount or another loop method
        print("Would you like to buy {a}?")
        input(">> ")
    elif choice == 'B' or choice == 'b':
        pass
    '''
    buyOption = input(">> ")
    if buyOption == '1':
        print("You don't want to buy this, it's junk!")
        input(">> ")
        storeMenu(playerig)
    elif buyOption == '2': # Dark Robes
        print(f"you want to buy Dark Robes?\nY/N")
        buyConfirm = input(">> ")
        if buyConfirm == 'Y' or buyConfirm == 'y':
            if playerig.gold < 500:
                print("You no have the coin for this. Go away!")
                input(">> ")
                storeMenu(playerig)
            else:
                playerig.gold -= 500
                playerig.armor = 'Dark Robes'
                playerig.ac = 4
                input("Boomba thanks you!")
                storeMenu(playerig)
        else:
            storeMenu(playerig)
    elif buyOption == '3': # Shining Robes
        print(f"you want to buy the Shining Robes?\nY/N")
        buyConfirm = input(">> ")
        if buyConfirm == 'Y' or buyConfirm == 'y':
            if playerig.gold < 1000:
                print("You no have the coin for this. Go away!")
                input(">> ")
                storeMenu(playerig)
            else:
                playerig.gold -= 1000
                playerig.armor = 'Shining Robes'
                playerig.ac = 8
                input("Boomba thanks you!")
                storeMenu(playerig)
        else:
            storeMenu(playerig)
    elif buyOption == '4': #Dwarven Ringmail
        print(f"you want to buy the Dwarven Ringmail?\nY/N")
        buyConfirm = input(">> ")
        if buyConfirm == 'Y' or buyConfirm == 'y':
            if playerig.gold < 500:
                print("You no have the coin for this. Go away!")
                input(">> ")
                storeMenu(playerig)
            else:
                playerig.gold -= 500
                playerig.armor = 'Dwarven Ringmail'
                playerig.ac = 9
                input("Boomba thanks you!")
                storeMenu(playerig)
        else:
            storeMenu(playerig)
    elif buyOption == '5': # Shadow Ringmail
        print(f"you want to buy the Shadow Ringmail?\nY/N")
        buyConfirm = input(">> ")
        if buyConfirm == 'Y' or buyConfirm == 'y':
            if playerig.gold < 1000:
                print("You no have the coin for this. Go away!")
                input(">> ")
                storeMenu(playerig)
            else:
                playerig.gold -= 1000
                playerig.armor = 'Shadow Ringmail'
                playerig.ac = 14
                input("Boomba thanks you!")
                storeMenu(playerig)
        else:
            storeMenu(playerig)
    elif buyOption == '6': # Fine Plate
        print(f"you want to buy the Fine Plate?\nY/N")
        buyConfirm = input(">> ")
        if buyConfirm == 'Y' or buyConfirm == 'y':
            if playerig.gold < 750:
                print("You no have the coin for this. Go away!")
                input(">> ")
                storeMenu(playerig)
            else:
                playerig.gold -= 750
                playerig.armor = 'Fine Plate'
                playerig.ac = 15
                input("Boomba thanks you!")
                storeMenu(playerig)
        else:
            storeMenu(playerig)
    elif buyOption == '7': # Cobolt Plate
        print(f"you want to buy the Cobolt Plate?\nY/N")
        buyConfirm = input(">> ")
        if buyConfirm == 'Y' or buyConfirm == 'y':
            if playerig.gold < 1250:
                print("You no have the coin for this. Go away!")
                input(">> ")
                storeMenu(playerig)
            else:
                playerig.gold -= 1250
                playerig.armor = 'Cobolt Plate'
                playerig.ac = 20
                input("Boomba thanks you!")
                storeMenu(playerig)
        else:
            storeMenu(playerig)
    elif buyOption == '8': # Iron Sword
        print(f"you want to buy the Iron Sword?\nY/N")
        buyConfirm = input(">> ")
        if buyConfirm == 'Y' or buyConfirm == 'y':
            if playerig.gold < 350:
                print("You no have the coin for this. Go away!")
                input(">> ")
                storeMenu(playerig)
            else:
                playerig.gold -= 350
                playerig.weapon = 'Iron Sword'
                playerig.atkbonus = 5
                input("Boomba thanks you!")
                storeMenu(playerig)
        else:
            storeMenu(playerig)
    elif buyOption == '9': # Steel Sword
        print(f"you want to buy the Steel Sword?\nY/N")
        buyConfirm = input(">> ")
        if buyConfirm == 'Y' or buyConfirm == 'y':
            if playerig.gold < 600:
                print("You no have the coin for this. Go away!")
                input(">> ")
                storeMenu(playerig)
            else:
                playerig.gold -= 600
                playerig.weapon = 'Steel Sword'
                playerig.atkbonus = 9
                input("Boomba thanks you!")
                storeMenu(playerig)
        else:
            storeMenu(playerig)
    elif buyOption == '10': # Iron Daggers
        print(f"you want to buy the Iron Daggers?\nY/N")
        buyConfirm = input(">> ")
        if buyConfirm == 'Y' or buyConfirm == 'y':
            if playerig.gold < 450:
                print("You no have the coin for this. Go away!")
                input(">> ")
                storeMenu(playerig)
            else:
                playerig.gold -= 450
                playerig.weapon = 'Iron Daggers'
                playerig.atkbonus = 9
                input("Boomba thanks you!")
                storeMenu(playerig)
        else:
            storeMenu(playerig)
    elif buyOption == '11': # Steel Daggers
        print(f"you want to buy the Steel Daggers?\nY/N")
        buyConfirm = input(">> ")
        if buyConfirm == 'Y' or buyConfirm == 'y':
            if playerig.gold < 700:
                print("You no have the coin for this. Go away!")
                input(">> ")
                storeMenu(playerig)
            else:
                playerig.gold -= 700
                playerig.weapon = 'Steel Daggers'
                playerig.atkbonus = 9
                input("Boomba thanks you!")
                storeMenu(playerig)
        else:
            storeMenu(playerig)
    elif buyOption == '13':
        print(f"you want to buy a healing potion?\nY/N")
        buyConfirm = input(">> ")
        if buyConfirm == 'Y' or buyConfirm == 'y':
            if playerig.gold < 25:
                print("You no have the coin for this. Go away!")
                input(">> ")
                storeMenu(playerig)
            else:
                playerig.gold -= 25
                playerig.potions +=1
                input("Boomba Thanks you!\n>> ")
                storeMenu(playerig)
        else:
            storeMenu(playerig)
    elif buyOption == '14':
        pass
    elif buyOption == 'B' or buyOption == 'b':
        pass #I don't like that this works... it just kills the script basically.. is this okay?
    elif buyOption == 's' or buyOption == 'S':
        storeMenu(playerig)
    else:
        input("Boomba not understand what tiny person want.\n**Press ENTER**")
        storeMenu(playerig)
'''
def testthings(payerig):
    armorList = ['Dusty Tunic', 'Dark robes', 'Shining Robes', 'Dwarven Ringmail', 'Shadow Ringmail', 'Fine Plate', 'Cobalt Plate']
    armorCost = [25, 500, 1000, 500, 1000, 750, 1250]
    acBonus = [1, 4, 8, 9, 14, 15, 20]
    aclassuse = ['Any', 'Mage', 'Mage', 'Rogue', 'Rogue', 'Warrior', 'Warrior']
    weaponList = ['Iron Sword', 'Steel Sword', 'Iron Daggers', 'Steel Daggers', 'Sacrificial Dagger']
    weaponCost = [350, 600, 350, 700, 500]
    wclassuse = ['Warrior', 'Warrior', 'Rogue', 'Rogue', 'Mage']
    dmgBonus = [5, 9, 9, 13, 6]
    itemList = ['Healing Potion', 'Greater Healing Potion']
    itemCost = [25, 125]
    itemCount = 1
    print(f"\nYour Gold: {playerig.gold}")
    for a, b, c, d in zip(armorList, armorCost, aclassuse, acBonus):
        print(f"{itemCount}. {a}: {b} Gold - AC Bonus: {d} - Class: {c}") #TESTING
        itemCount += 1
    for d, e, f, g in zip(weaponList, weaponCost, wclassuse, dmgBonus):
        print(f"{itemCount}. {d}: {e} Gold - Damage Bonus: {g} - Class: {f}") #TESTING
        itemCount += 1
    for g, h in zip(itemList, itemCost):
        print(f"{itemCount}. {g}: {h} Gold") #TESTING
        itemCount += 1
        #finish the rest below and fix the above prints
    print("S. Sell")
    print("B. Back")
    choice = input(">> ")
    if choice in str(itemCount):
        print("Would you like to buy {a}?")
        input(">> ")
    elif choice == 'B' or choice == 'b':
        pass
    

'''

if __name__ == '__main__':
    #testthings()
    storeMenu()