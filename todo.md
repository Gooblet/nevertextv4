# TODO Tracker

## Next Coding Session Notes
###############################################################
Line 123 in nevertext.py 
    - this is the example i need to go through all the code and test the clear function
    - then i need to test another more complex function and move it to its own file

https://www.devdungeon.com/content/colorize-terminal-output-python
color text like HP and skills or if a count = 0 where it makes sense.
do this to draw attention to important stats.
This is going to be complicated with how I have things organized.

Traveling needs to be a thing. 
###############################################################


## Longer Term goals
Code updates:
    - Redesign nevertext.py to be much shorter
        - functions should be in their own file and i import those files and call the functions
            - I remember having issues with this so i need to test it on small/basic functions like the clear screen one first
                

### New Features
    - Meditate to restore skill points -- added
        - meditate can trigger combat during meditation (do a loop, +1 skill point and run a ran encounter function)
    - Add Lore to the bosses 
        - add an intro screen to each boss (bosses can only be killed once, need to add a flag to save file once boss is dead and append a *defeated* flag)

### Combat section:
    - alpha Tier 1 stuff created
        - add other tiers of enemies
        - load tiers based on player level
        - could have multiple spreadsheets for players who just hit the second tier
            - IE: when player hits level 6, then load the level 6 enemies who are harder but they aren't going to run into a level 10 enemy right away

### New Character:
    - New character creation:: need to check and see if a .csv file with the character's name exist in the current directory.
        - if that character exists, report to player and make them pick another name.

    - start working on loot (I think i need to make the save feature work before i do this because i need to save loot and know how to assign the loot to the save function)

    - add store system. just selling potions for now

    - add a section in create character that lets the player reroll stats or pick another character before starting the game


    - fix the lore menu so it doesn't print everything out at once.
        - this could just easily be done by splitting the files by paragraph and adding submenues to the functions
            - this will take time to redo the lore into multiple files
                - rough estimate could be over 100 lore files.
        - work on finding a way to read out the text files by line and press ENTER to keep reading or Q to quit

### BOSS fights
    - need to work on balancing the fights
    - also add in a check to see if the player is a high enough level (could also check specific stats like str or stamina)
        - don't stop the player from trying, but give a warning if the player is too weak

### Tutorial
    - Build a tutorial into the user interface of the game for each time a menu is explored -- less hand holdy.
        - all tutorial screens should be accessible through a help menu.



Concept changes
This section is going to focus on things that i eventually want to tackle as i work through the core functions of the game

- Mage should do the most damage overall but should be the squishiest. 
- Warrior should be the most stable for leveling but will struggle without good gear for boss fights
    - Need to add in magic weapons. 
    - some mobs will be 'magic' or basically resistant to basic attacks without a magic weapon. whole magic system needs lots of work still.
- rogues should do the most consistent damage (IE: while the mages have the biggest nukes, the rogues can backstab multiple times)

NOTE: this is going to require a rework of the leveling functions to add more skill points to the rogues

Currently, all classes start with the same overall design. I need to add more skill points to the mage and rogue and let the warrior and cleric have the fewest number of skill points. The reason the cleric is going to have a lower number is because they can both heal and use potions.

Crafting:
    Want to do crafting eventually, but i need to have a fully functional loot system and the save game must be working so i can save the player's inventory.... 
    - crafting ideas:
        - better armor/weapons
        - potions
        - some epic items will require crafting to work

COMBAT:
i believe the characters are potentially overpowered. before i tweak anything else in combat more, i need to add another enemy besides the skeleton.
if i create lists of enemies for testing and do the same thing for the enemy selection as i do for the player assignment(which i need to improve here) then i could pick enemies
    - With current modifications, warrior dies relatively often if out of skill points. resting is not dangerous enough, there is little risk. need to edit bash function.

CHARACTER CREATION:
i do the for x in y loop, i should be able to assign the variables based on that loop right?
need to work that out eventually... then i can use that function to assign the values to the enemies.

TRAVEL:
Going to need to build a lot of new stuff to make this work i feel. Character's current location, where they can travel to, etc.


- Want to build enemies that live in specific areas as well. (later addition once i get major areas developed.)

- add traveling (add dungeons as well)
    - random events while traveling ('you notice a cave to the right', 'you were attacked by a bandit')

