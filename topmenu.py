import os
import sys

def statMenu(playerig):
    #This is just the top menu for the player's  stats that can be called to display on all screens
    print("###################################################################################")
    print(f"Name: {playerig.name:<13}\t|\tLevel: {playerig.level:<13}\t")
    print(f"HP: {playerig.health} / {playerig.maxhealth:<10}\t|\tClass: {playerig.pclass:<10}\t")
    print(f"Gold: {playerig.gold:<13}\t|\tExperience: {playerig.exp} / {playerig.levelupxp:<13}\t")
    print(f"Weapon: {playerig.weapon:<13}\t|\tPotions: {playerig.potions:<13}\t")
    print(f"Armor: {playerig.armor:<10}\t|\t\t")
    print(f"Skill Points: {playerig.currentskills} / {playerig.maxskills:<4}\t|\t Buff Armor: {playerig.buffDuration:<4}\t")
    print("###################################################################################")


if __name__ == '__main__':
    statMenu()